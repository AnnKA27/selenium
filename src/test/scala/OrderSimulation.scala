import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class OrderSimulation extends Simulation {
  private val baseUrl = "http://003.jborn.tech"
  private val basicAuthHeader = "Basic YWRtaW46cGFzc3dvcmQ="
  private val contentType = "application/json"
  private val endpoint = "/orders"

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl(baseUrl)
    .authorizationHeader(basicAuthHeader)
    .contentTypeHeader(contentType)

  val scn: ScenarioBuilder = scenario("OrderSimulation")
    .exec(
      http("OrderRequest")
        .post(endpoint)
        .body(RawFileBody("src/main/resources/request.json"))
        .check(status.is(200))
    )

  setUp(
    scn.inject(
      constantUsersPerSec(26).during(30 seconds)
    )
  ).protocols(httpProtocol)

}
