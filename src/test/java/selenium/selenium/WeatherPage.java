package selenium.selenium;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.codeborne.selenide.Selenide.$;

public class WeatherPage {

    @FindBy(how = How.CSS, using = ".CurrentConditions--tempValue--3KcTQ")
    private SelenideElement temperature;

    public Integer temperatureElement() {
        return Integer.valueOf($(temperature).innerText().substring(0,2));
    }
}
