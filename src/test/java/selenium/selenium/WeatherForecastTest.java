package selenium.selenium;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
public class WeatherForecastTest {

    @Test
    public void testTemperature() {
        WeatherPage page = open("https://weather.com/weather/today/l/60.02,30.6?par=google&temp=c", WeatherPage.class);
        if (page.temperatureElement() > 0) {
            log.info("Тепло");
        } else {
            fail("Холодно");
        }
    }
}

